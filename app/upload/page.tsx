'use client'
import React, { useState } from 'react'
import { CldUploadWidget, CldImage } from 'next-cloudinary'

interface CloudinaryResult {
    public_id: string;
}

const UploadPage = () => {
    const [publicId, setPublicId] = useState('');

  return (
    <>
    {publicId && <CldImage src={publicId} width={304} height={171} alt="My desktop" />}
    <CldUploadWidget 
    uploadPreset='nge8jyyv'
    options={{
      sources: ['local'],
      cropping: true,
      multiple: false,
      maxFiles: 5,
      styles: {
        palette: {
            window: "#3A1013",
            sourceBg: "#20304b",
            windowBorder: "#7171D0",
            tabIcon: "#79F7FF",
            inactiveTabIcon: "#8E9FBF",
            menuIcons: "#CCE8FF",
            link: "#72F1FF",
            action: "#5333FF",
            inProgress: "#00ffcc",
            complete: "#33ff00",
            error: "#cc3333",
            textDark: "#000000",
            textLight: "#ffffff"
        },
        fonts: {
            default: null,
            "'Space Mono', monospace": {
                url: "https://fonts.googleapis.com/css?family=Space+Mono",
                active: true
            }
        }
    }
    
    }}
    onUpload={(result, widget) => {
        if (result.event !== 'success') return;
 const info = result.info as CloudinaryResult;
        setPublicId(info.public_id) 
    }}>
       {({ open }) => 
       <button 
            className='btn btn-primary'
            onClick={() => open()}>Upload</button>}
    </CldUploadWidget>
    </>
  )
}

export default UploadPage