import { NextRequest, NextResponse } from "next/server";
import schema from "../schema";
import prisma from "@/prisma/client";

export async function GET(
    request: NextRequest, 
    { params }: { params: { id: string } }) {
        const user = await prisma.user.findUnique({
            where: {id: params.id}
    });

            // Fetch data from a db
            // If not found, return 404 error
            if (!user)
                return NextResponse.json({ error: 'dati non trovati' }, { status: 404 })

            // Else return data 
            return NextResponse.json(user);
    }

    export async function PUT (
        request: NextRequest, 
        { params }: { params: { id: string } }) {
            // Validate il body della request
            const body = await request.json();
            const validation = schema.safeParse(body);
            if (!validation.success) // If invalid, return 400
                return NextResponse.json(validation.error.errors, { status: 400 });
            // return NextResponse.json({ error: 'Name is required' }, { status: 400 });

            const user = await prisma.user.findUnique({
                where: {
                    id: params.id
                }
            });

            // Fetch the user with given id (magari da DB)
            if (!user) // If doesn't exist, return 404
                return NextResponse.json(
                    { error: 'User not found (anche db)' }, 
                    { status: 404 }
                );

            const updateUser = await prisma.user.update({
                where: { id: user.id },
                data: {
                    name: body.name,
                    email: body.email
                }
            });
            
            // Upload the user
            // Return the upload user 
            return NextResponse.json( updateUser );
        }

        export async function DELETE( 
            request: NextRequest, 
            { params }: { params: { id: string } }
        ) {
            const user = await prisma.user.findUnique({
                where: {
                    id: params.id
                }
            });
            // Fetch user from db
            if (!user) // If not found, return 404
                return NextResponse.json(
                    {error: 'User not found'}, 
                    {status: 404}
                );

            await prisma.user.delete({
                where: { id: user.id }
            });

            // Delete the user
            return NextResponse.json({ });
            // Return 200
        }
        