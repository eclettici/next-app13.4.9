import React, { Suspense } from 'react'
import UserTable from './UserTable'
import Link from 'next/link'
import { Metadata } from 'next'

interface Props {
  searchParams: {sortOrder: string}
}

const UsersPage = async ({searchParams: { sortOrder } }: Props) => {
  console.log(sortOrder)

  return (
    <>
        <h1>Users</h1>
        <Link href="/users/new" className='btn'>New Users</Link>
        <Suspense fallback={<p>Loading...</p>}>
          <UserTable sortOrder={sortOrder} /> 
        </Suspense>
    </>
  )
}

export default UsersPage

export const metadata: Metadata = {
  title: 'User Page',
  description: 'dot'
}