export { default } from "next-auth/middleware";

export const config = {
    // *: zero o più parametri
    // +: uno o più parametri
    // ?: zero o un parametro
    matcher: ['/dashboard/:path*']
}